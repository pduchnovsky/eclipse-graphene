#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import json
import os
import shutil
from io import BytesIO
from typing import Dict, List

import numpy as np
from PIL import Image

from auto_sync_common.model import Pipeline, Model
from auto_sync_common.utils import APIClient, FileManager
from auto_sync_common.document_manager import DocumentManager


class SolutionDownloader(object):
    """
    This class reads the list of solution ids belonging to either models or pipelines.
    And downloads all artifacts related to a solution.
    This class can be configured from a json file, and it downloads solutions from the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """

    def __init__(
            self
    ) -> None:
        # setup parameters from reading environment variables.
        self.SRC_GRAPHENE_HOST = os.environ['SRC_GRAPHENE_HOST']  # FQHN like aiexp-preprod.ai4europe.eu
        self.REGISTRY_HOST = os.environ['REGISTRY_HOST']
        self.SRC_GRAPHENE_TOKEN = os.environ['SRC_GRAPHENE_TOKEN']  # format is 'graphene_username:API_TOKEN'
        self.SRC_GRAPHENE_USER = os.environ['SRC_GRAPHENE_USER']
        self.SRC_GRAPHENE_PW = os.environ['SRC_GRAPHENE_PW']
        self.SRC_GRAPHENE_CATALOGID = os.environ['SRC_GRAPHENE_CATALOGID']
        self.SRC_GRAPHENE_USERID = os.environ['SRC_GRAPHENE_USERID']
        self.SRC_NEXUS_RW_USERNAME = os.environ['SRC_NEXUS_RW_USERNAME']
        self.SRC_NEXUS_RW_PASSWORD = os.environ['SRC_NEXUS_RW_PASSWORD']
        self.SRC_NEXUS_HOST = os.environ['SRC_NEXUS_HOST']
        self.SRC_NEXUS_PORT = os.environ['SRC_NEXUS_PORT']
        self.SRC_NEXUS_REPO_NAME = os.environ['SRC_NEXUS_REPO_NAME']
        self.SRC_NEXUS_ROOT_GROUP_ID = os.environ['SRC_NEXUS_ROOT_GROUP_ID']
        self.SRC_GRAPHENE_DOC_USERID = os.environ['SRC_GRAPHENE_DOC_USERID']
        self.docker_base = f'{self.REGISTRY_HOST}:7444/ai4eu-experiments/openml'
        self.ret_dir_name = "retrieved"
        self.header_type = {
            "Content-Type": "application/json",
            "Authorization": self.SRC_GRAPHENE_TOKEN,
        }
        self.host = f'https://{self.SRC_GRAPHENE_HOST}'
        self.api_client = APIClient(self.SRC_GRAPHENE_USER, self.SRC_GRAPHENE_PW, self.host)
        self.document_mgr = DocumentManager()
        self.get_document_manager()

    def get_document_manager(self):
        doc_manager_details = {
            'NEXUS_RW_USERNAME': self.SRC_NEXUS_RW_USERNAME,
            'NEXUS_RW_PASSWORD': self.SRC_NEXUS_RW_PASSWORD,
            'NEXUS_HOST': self.SRC_NEXUS_HOST,
            'NEXUS_PORT': self.SRC_NEXUS_PORT,
            'NEXUS_REPO_NAME': self.SRC_NEXUS_REPO_NAME,
            'NEXUS_ROOT_GROUP_ID': self.SRC_NEXUS_ROOT_GROUP_ID
        }
        self.document_mgr.init_with_details(doc_manager_details)

    def get_solution_list(self) -> Dict:
        """
        Retrieves the list of solutions (model/pipeline) from the graphene host.
        :return: dictionary object with relevant details of all the solutions available on the source graphene host.
        """
        resp = self.api_client.get_request(f"ccds/solution")
        return resp

    def get_solution(self, solution_id: str) -> Dict:
        """
        Retrieves the solution (model/pipeline) from the source graphene host.
        :param solution_id: solution identifier to retrieve appropriate model/pipeline from the graphene host.
        :return: dictionary object with relevant details of the retrieved solution.
        """
        resp = self.api_client.get_request(f"ccds/solution/{solution_id}")
        return resp

    def get_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: the latest revision number for the onboarded solution.
        """
        revision_api = f'ccds/solution/{solution_id}/revision'
        response = self.api_client.get_request(revision_api)
        if response:
            version = np.array([i['version'] for i in response]).argmax()
            return response[version]['revisionId']
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0

    def get_solution_artifacts(self, revision_id):
        artifacts_api = f'ccds/revision/{revision_id}/artifact'
        response = self.api_client.get_request(artifacts_api)
        return response

    def get_authors_publisher(self, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to get the author and publisher details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the author and publisher details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        author_api = f'ccds/solution/{solution_id}/revision/{revision_id}'
        response = self.api_client.get_request(author_api)
        authors = None
        publisher = None

        if "authors" in response:
            authors = response["authors"]
        if "publisher" in response:
            publisher = response["publisher"]
        print(f'Authors and Publisher: \n{authors}\n{publisher}')
        return {'authors': authors, "publisher": publisher}

    def get_description(self, revision_id: str, catalog_id: str) -> Dict:
        """
        This method calls an api to get the solution description of the solution.
        :param revision_id: current revision number for which we need to get the description.
        :param catalog_id: catalog identifier in which the solution is available.
        :return: string that represents the solution's description.
        """
        desc_api = f'ccds/revision/{revision_id}/catalog/{catalog_id}/descr'
        response = self.api_client.get_request(desc_api)
        description = None
        if "description" in response:
            description = response["description"]
        print(f'Setup Description Status: {description}')
        return {"description": description}

    def get_tags(self, solution_id: str, revision_id: str) -> List[Dict]:
        """
        This method calls an api to get the tags of the solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the tags.
        :return: list of tags.
        """
        tag_api = f'ccds/solution/{solution_id}'
        response = self.api_client.get_request(tag_api)
        if 'tags' in response:
            tags = response['tags']
            return {"tags": tags}
        return {"tags": None}

    def get_icon(self, solution_id: str) -> bool:
        """
        This method calls an api to get the icon of the solution.
        :param solution_id: solution identifier for the solution.
        :return: bytes string that represent the solution's icon.
        """
        icon_api = f'ccds/solution/{solution_id}/pic'
        icon = self.api_client.get_image(icon_api)
        return icon

    def store_icon(self, icon: bytes, folder_location: str):
        """
        Store the icon available in bytes format to the provided directory.
        :param icon: the bytes string representation of the solution's icon. The icon will be stored either as `icon.png` or `icon.jpg` depending upon the file retrieved from server.
        :param folder_location: location of the solution folder
        Reference: https://stackoverflow.com/a/23489503
        """
        file_name = None
        if "PNG" in str(icon):
            file_name = "icon.png"
        else:
            file_name = "icon.jpg"
        icon_img = Image.open(BytesIO(icon))
        icon_img.save(os.path.join(folder_location, file_name))
        return file_name

    def create_retrieval_dir(self, delete_old_files=False):
        """
        Creates a directory to store all the retrieved solutions.
        :param delete_old_files: a flag, when set True, deletes all previous solutions in the retreival directory.
        """
        if os.path.exists(self.ret_dir_name):
            if delete_old_files:
                shutil.rmtree(self.ret_dir_name)
            else:
                print("Not deleting old content")
        os.makedirs(self.ret_dir_name, exist_ok=True)

    def get_cdump_blueprint(self, pipeline_revision_id: Pipeline, pipeline_location):
        """
        This method downloads cdump.json and blueprint.json of the published the pipeline (composite solution).
        :param pipeline: holds required details of the pipeline.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        get_rev_artifact_api = "ccds/revision/{REVISION_ID}/artifact"
        get_rev_artifact_api = get_rev_artifact_api.replace("{REVISION_ID}", pipeline_revision_id)
        comp_sol_resp = self.api_client.get_request(get_rev_artifact_api)
        blueprint_artifact_uri = None
        blueprint_artifact = None
        cdump_artifact_uri = None
        cdump_artifact = None
        save_blueprint = False
        save_cdump = False
        for element in comp_sol_resp:
            if element['artifactTypeCode'] == "BP":
                blueprint_artifact_uri = element["uri"]
                blueprint_artifact = self.document_mgr.get_document(blueprint_artifact_uri)
                with open(os.path.join(pipeline_location, 'blueprint.json'), 'w') as bp_file:
                    json.dump(blueprint_artifact['content'].decode("utf-8"), bp_file)
                save_blueprint = True
            if element['artifactTypeCode'] == "CD":
                cdump_artifact_uri = element["uri"]
                cdump_artifact = self.document_mgr.get_document(cdump_artifact_uri)
                with open(os.path.join(pipeline_location, 'cdump.json'), 'w') as cd_file:
                    json.dump(cdump_artifact['content'].decode("utf-8"), cd_file)
                save_cdump = True
        # pipeline.cdump = json.loads(cdump_artifact['content'])
        # pipeline.blueprint = json.loads(blueprint_artifact['content'])
        save_result = save_cdump and save_blueprint
        return save_result

    def download_solution(self, solution_id: str) -> bool:
        """
        Downloads the solution and stores all its corresponding files in a folder with name of the solution.
        :param solution_id: solution identifier to download details of the solution.
        :return: boolean value, where True means download was success, False meaning failure occured.
        """
        self.create_retrieval_dir(False)
        solution = self.get_solution(solution_id)
        revision_id = self.get_revision_id(solution_id)
        icon = self.get_icon(solution_id)
        tags = self.get_tags(solution_id, revision_id)
        description = self.get_description(revision_id, soln_retriever.GRAPHENE_CATALOGID)
        authors = self.get_authors_publisher(solution_id, revision_id)
        solution = solution | tags | description | authors
        is_model = False
        soln_object = None
        if 'toolkitTypeCode' in solution:
            soln_name = solution['name']
            if solution['toolkitTypeCode'] == 'CP':
                soln_path = os.path.join(self.ret_dir_name, soln_name)
                soln_object = Pipeline(soln_name, solution, soln_path, True)
                soln_object
            else:
                print("Model")
                soln_path = os.path.join(self.ret_dir_name, soln_name)
                soln_object = Pipeline(soln_name, solution, soln_path, True)

    def get_pipelines(self):
        solutions_list = self.get_solution_list()
        pipelines_list = []
        for solution in solutions_list['content']:
            if 'toolkitTypeCode' in solution and solution['toolkitTypeCode'] == 'CP':
                pipelines_list.append(solution)
        return pipelines_list

    def get_child_nodes_of_pipeline(self, pipeline_solution_id):
        """
        This method gives a list of solution ids, that are part of the pipeline.
        :param pipeline_solution_id: solution id of the pipeline.
        :return: list of solutions ids, of all the member nodes of pipeline.
        """
        child_solution_ids = []
        api_link = f"ccds/solution/{pipeline_solution_id}/comp"
        child_solution_ids = self.api_client.get_request(api_link)
        return child_solution_ids

    def prepare_folder_structure_pipeline(self):
        curr_location = os.path.dirname(__file__)
        is_download_folder_created = FileManager.create_directory(curr_location, "downloaded_solutions")
        download_folder_location = os.path.join(curr_location, "downloaded_solutions")

    def prepare_folder_structure_model(self):
        pass

    def download_solution(self):
        pass

    def download_pipeline(self, pipeline_solution_id):
        pipeline_details = self.get_solution(solution_id=pipeline_solution_id)
        pipeline_revision_id = self.get_revision_id(pipeline_solution_id)
        pipeline_name = pipeline_details['name']
        children_solution_ids = self.get_child_nodes_of_pipeline(pipeline_solution_id)
        curr_dir_location = os.path.dirname(__file__)
        download_folder_location= os.path.join(curr_dir_location, 'downloaded_solutions')
        pipeline_location = os.path.join(download_folder_location, pipeline_name)

        FileManager.create_directory(download_folder_location, pipeline_name)
        self.get_cdump_blueprint(pipeline_revision_id,pipeline_location)
        children_models = [self.download_model(model_solution_id, pipeline_location) for model_solution_id in
                           children_solution_ids]
        # TODO: add_link_params to be fixed in the following json
        solutions_details = {
            "pipeline_name": pipeline_name,
            "location": pipeline_location,
            "models": [model.name for model in children_models],
            "icon_filename": "icon.png",
            "tags": {
                "modelTypeCode": "RG",
                "toolkitTypeCode": "CP",
                'tags': pipeline_details['tags']
            },
            "documents": [],
            "add_link_params": [],
            "modify_node_urls": [
                "dsce/dsce/solution/modifyNode?userid={GRAPHENE_USER_ID}&cid={COMP_SOLUTION_ID}&nodeid=hpp-predictor-16992235121&nodename=hpp-predictor-16992235121&ndata={\"fixed\":false}"
            ],
            "add_link_url": "dsce/dsce/solution/addLink"
        }
        pipeline = Pipeline(solution_name=pipeline_name, solution_details=solutions_details, local_fpath='.',
                            is_retrieved=True)

    def download_model(self, model_solution_id, model_parent_dir):
        model_solution_details = self.get_solution(solution_id=model_solution_id)
        model_name = model_solution_details['name']
        model_revision_id = self.get_revision_id(model_solution_id)
        model_artifacts = self.get_solution_artifacts(model_revision_id)
        docker_image_uri = None
        license_uri = None
        protobuf_uri = None
        for artifact in model_artifacts:
            if artifact['name'] == model_name:
                docker_image_uri = artifact['uri']
            if artifact['name'] == "license.json":
                license_uri = artifact['uri']
            if artifact['name'] == "model.proto":
                protobuf_uri = artifact['uri']
        print(docker_image_uri)
        license_json_artifact = self.document_mgr.get_document(license_uri)
        protobuf_artifact = self.document_mgr.get_document(protobuf_uri)

        # curr_location = os.path.dirname(__file__)
        # is_download_folder_created = FileManager.create_directory(model_parent_dir, "downloaded_solutions")
        download_folder_location = model_parent_dir
        solution_download_location = os.path.join(download_folder_location, model_name)
        is_solution_folder_created = FileManager.create_directory(download_folder_location, model_name)
        license_file_path = os.path.join(solution_download_location, "license.json")
        protobuf_file_path = os.path.join(solution_download_location, "model.proto")
        with open(license_file_path, 'wb') as license_file:
            license_file.write(license_json_artifact['content'])
        with open(protobuf_file_path, 'wb') as protobuf_file:
            protobuf_file.write(protobuf_artifact['content'])
        model_icon = self.get_icon(model_solution_id)
        icon_file_name = self.store_icon(model_icon, solution_download_location)
        model_description = self.get_description(model_revision_id, self.SRC_GRAPHENE_CATALOGID)
        description_file_path = os.path.join(solution_download_location, "description.txt")
        with open(description_file_path, 'w') as description_file:
            description_file.write(model_description['description'])

        solution_dict = {
            'model_name': model_name,
            'docker_uri': docker_image_uri,
            "category": model_solution_details['modelTypeCode'],
            'documents': [],
            'tags': {
                "modelTypeCode": model_solution_details['modelTypeCode'],
                'toolkitTypeCode': model_solution_details['toolkitTypeCode'],
                'tags': model_solution_details['tags']
            },
            "icon_filename": icon_file_name,
            "protobuf": "model.proto",
            "license": "license.json",
            "nodeId": "tensorboard-16992235121",
            "nodeSolutionId": "19ef5147-cf96-4817-8f36-1c125df45376"
        }
        solution_json_location = os.path.join(solution_download_location, 'solution.json')
        with open(solution_json_location, 'w') as sol_json_file:
            json.dump(solution_dict, sol_json_file)

        author_details = self.get_authors_publisher(model_solution_id, model_revision_id)
        model_object = Model(model_name, solution_dict, solution_download_location, True)
        return model_object


def download_model():
    model_name = "hpp-db"
    solution_downloader = SolutionDownloader()
    solution_details = solution_downloader.get_solution_list()['content'][0]
    model_solution_id = solution_details['solutionId']
    model_name = solution_details['name']
    solution = solution_downloader.get_solution(solution_id=model_solution_id)
    solution_revision_id = solution_downloader.get_revision_id(model_solution_id)
    solution_artifacts = solution_downloader.get_solution_artifacts(solution_revision_id)
    print(solution_artifacts)

    docker_image_uri = None
    license_uri = None
    protobuf_uri = None
    for artifact in solution_artifacts:
        if artifact['name'] == model_name:
            docker_image_uri = artifact['uri']
        if artifact['name'] == "license.json":
            license_uri = artifact['uri']
        if artifact['name'] == "model.proto":
            protobuf_uri = artifact['uri']
    print(docker_image_uri)
    license_json_artifact = solution_downloader.document_mgr.get_document(license_uri)
    protobuf_artifact = solution_downloader.document_mgr.get_document(protobuf_uri)

    curr_location = os.path.dirname(__file__)
    is_download_folder_created = FileManager.create_directory(curr_location, "downloaded_solutions")
    download_folder_location = os.path.join(curr_location, "downloaded_solutions")
    solution_download_location = os.path.join(download_folder_location, model_name)
    is_solution_folder_created = FileManager.create_directory(download_folder_location, model_name)
    license_file_path = os.path.join(solution_download_location, "license.json")
    protobuf_file_path = os.path.join(solution_download_location, "model.proto")
    with open(license_file_path, 'wb') as license_file:
        license_file.write(license_json_artifact['content'])
    with open(protobuf_file_path, 'wb') as protobuf_file:
        protobuf_file.write(protobuf_artifact['content'])
    model_icon = solution_downloader.get_icon(model_solution_id)
    solution_downloader.store_icon(model_icon, solution_download_location)
    model_description = solution_downloader.get_description(solution_revision_id,
                                                            solution_downloader.SRC_GRAPHENE_CATALOGID)
    description_file_path = os.path.join(solution_download_location, "description.txt")
    with open(description_file_path, 'w') as description_file:
        description_file.write(model_description['description'])

    solution_dict = {
        'model_name': model_name,
        'docker_uri': docker_image_uri,
        "category": solution_details['modelTypeCode'],
        'documents': [],
        'tags': {
            "modelTypeCode": solution_details['modelTypeCode'],
            'toolkitTypeCode': solution_details['toolkitTypeCode'],
            'tags': solution_details['tags']
        },
        "icon_filename": "icon.jpg",
        "protobuf": "model.proto",
        "license": "license.json",
        "nodeId": "tensorboard-16992235121",
        "nodeSolutionId": "19ef5147-cf96-4817-8f36-1c125df45376"
    }
    solution_json_location = os.path.join(solution_download_location, 'solution.json')
    with open(solution_json_location, 'w') as sol_json_file:
        json.dump(solution_dict, sol_json_file)

    author_details = solution_downloader.get_authors_publisher(model_solution_id, solution_revision_id)

    print("Downloaded Solution!")


def download_pipeline():
    solution_downloader = SolutionDownloader()
    pipelines_list = solution_downloader.get_pipelines()
    sample_pipeline = pipelines_list[0]
    pipeline_solution_id = sample_pipeline['solutionId']
    # children_solution_ids = solution_downloader.get_child_nodes_of_pipeline(pipeline_solution_id)
    # children_solution_ids
    solution_downloader.download_pipeline(pipeline_solution_id)


if __name__ == "__main__":
    # solution_id = "8b965f7a-2787-41cf-9980-0a2bcefb0d01"
    # solution_id = "f88d26a5-53a1-45b4-a587-5f0481da1884"
    # solution_id = "e33a3ded-b857-4f2c-8055-5c73662a678b"
    # solution_id = '8932a236-a024-4eda-ac5a-bf68b59543ad'
    # solution_id = "41218ad2-efe7-4822-b632-d52dcc2ad150"
    # soln_downloader = SolutionDownloader()
    # soln = soln_downloader.get_solution_list()
    print("Downloaded Solution")
    download_pipeline()
