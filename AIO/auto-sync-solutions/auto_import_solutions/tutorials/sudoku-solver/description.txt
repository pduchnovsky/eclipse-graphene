This is the streaming version of the deployable Solution of the AI4EU Experiments Sudoku Hello World!

It is a Proof of Concept for a Sudoku design assistant based on ASP, gRPC, and Protobuf, deployable in the AI4EU Experiments Platform.

The Git repository holding this component of the Hello World is publicly available here: https://github.com/peschue/ai4eu-sudoku

A Tutorial video about this "Sudoku Hello World" can be found here: https://www.youtube.com/watch?v=gM-HRMNOi4w