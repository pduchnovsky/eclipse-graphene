#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import os
from auto_import_solutions import solution_uploader
from auto_export_solutions import solution_downloader
import argparse
from auto_sync_common.utils import EnvVarsManager

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Auto-Sync CLI Tool')
    parser.add_argument('-e', '--env-vars',
                        help='Root Directory where eclipse-graphene is installed. ' +
                             'This will help in locating the environment variables and uploading tutorials.' +
                             'Default value would be "/home/ai4eu/eclipse-graphene"',
                        required=False, default="/home/ai4eu/eclipse-graphene/AIO/")
    args = parser.parse_args()
    env_vars_mgr = EnvVarsManager(args.env_vars)
    target_env_vars = env_vars_mgr.target_env_config
    IS_TUTORIAL_MODE = ('SYNC_MODE' in target_env_vars) and ("TUTORIALS" == target_env_vars["SYNC_MODE"])
    IS_DOWNLOAD_MODE = ('SYNC_MODE' in os.environ) and ("DOWNLOAD" == os.environ["SYNC_MODE"])
    if IS_TUTORIAL_MODE:
        solution_uploader.upload_tutorials(target_env_vars)
    if IS_DOWNLOAD_MODE:
        # solution_downloader.download_model()
        solution_downloader.download_pipeline()