#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2018 AT&T Intellectual Property. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: script to setup the mariadb for Graphene, under docker
#
# Prerequisites:
# - Graphene core components through oneclick_deploy.sh
#
# Usage:
# For docker-based deployments, run this script on the AIO host.
# $ bash setup_mariadb.sh
#
# NOTE: Redeploying MariaDB with an existing DB is not yet supported
#

function setup_mariadb() {
  trap 'fail' ERR
  cd $AIO_ROOT/../charts/mariadb
  bash setup_mariadb.sh clean $GRAPHENE_MARIADB_HOST $K8S_DIST
  bash setup_mariadb.sh setup $GRAPHENE_MARIADB_HOST $K8S_DIST
}

set -x
trap 'fail' ERR
WORK_DIR=$(pwd)
cd $(dirname "$0")
if [[ -z "$AIO_ROOT" ]]; then export AIO_ROOT="$(cd ..; pwd -P)"; fi
source $AIO_ROOT/utils.sh
source $AIO_ROOT/graphene_env.sh
setup_mariadb
cd $WORK_DIR
