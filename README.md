<!---
.. ===============LICENSE_START=======================================================
.. Graphene CC-BY-4.0
.. ===================================================================================
.. Copyright (C) 2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
.. ===================================================================================
.. This Graphene documentation file is distributed by AT&T and Tech Mahindra
.. under the Creative Commons Attribution 4.0 International License (the "License");
.. you may not use this file except in compliance with the License.
.. You may obtain a copy of the License at
..
..      http://creativecommons.org/licenses/by/4.0
..
.. This file is distributed on an "AS IS" BASIS,
.. WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.. See the License for the specific language governing permissions and
.. limitations under the License.
.. ===============LICENSE_END=========================================================
-->

# Eclipse Graphene Installation

This repository holds installation and deployment scripts for the Eclipse Graphene system.

Software versions:
* Kubernetes 1.26.10
* Kube CNI 1.2.0
* Kubernetes dashboard 2.6.1
* Calico 3.26.4
* Helm 3.12.3
* Ingress-Nginx Chart 4.7.1

Preconditions:
* Ubuntu 22.04 server, one big partition
* At least 20GB of disk space available recommended (/var/lib alone will require more than 10GB)
* Installation user created that belongs to group sudo, in this example the user is ai4eu
* /etc/hosts has exactly one entry for the FQHN pointing to the externel ipv4 interface
* optionally letsencrypt certificates installed

Clone this repo in the home folder of the installation user and then:

    # as root
    cd eclipse-graphene/tools
    bash setup_k8s_helm.sh 
    # Use the installation user without sudo (you will be asked for sudo if neccesary).
    cd $HOME
    # replace FQHN appropriately
    bash eclipse-graphene/AIO/setup_prereqs.sh k8s FQHN $USER generic | tee log_prereqs.txt
    cd eclipse-graphene/AIO/
    bash oneclick_deploy.sh | tee log_oneclick.txt

Some of those scripts might take several minutes to complete execution.
The last script should end with output showing the URLs to use, e.g. among others:

    Portal: https://(your FQHN):443


# Getting Started: Deploying Tutorial Pipelines

The basic installation of Eclipse Graphene, is an empty setup, with no available models or pipelines. To get started with Eclipse Graphene, there is a possibility to deploy some tutorial pipelines. With this, one can play around with these tutorials, and get inspired to created their own pipeline. In order to deploy these tutorial pipelines, you can follow the steps provided below:

1. Create a user in Graphene using an email and username. Give the publisher rights to your user.
2. Once the user is created, you need to create your user token. For this, first we need to get the API token of your account. To get this, perform the following steps:
   * Login to Graphene using newly created user. Go to the account setting.
   * Go to the section: API Token and copy it.
   * And then the User Token is created by joining the username from step 1, and APItoken with a colon (:)
     * For example `` `<username>:<api_token>` ``
     * This user token will be needed to invoke the script later.
3. Once we have the `user token`, open the terminal and navigate to the eclipse-graphene installation path.
   * The default path should be `/home/ai4eu/eclipse-graphene`.
   * Then go to `AIO/auto-sync-solutions` folder in the terminal.
   * Set the necessary information as the environment variable in your terminal. The required command would be:
     * `export GRAPHENE_TOKEN=<user_token>`
   * If you have the default installation directory, just run the following command:
     * `python auto_sync.py`
   * If you have a different installation directory, run the following command:
     * `python auto_sync.py -e <path_to eclipse-graphene directory/AIO>`
4. Once the script starts running, you can see the updates on the log. Later, as the script ends, in the browser refresh the design studio page, and you can see the onboarded tutorial pipelines in the solution section.
