#!/bin/bash

if [[ $(id -u) > 0 ]]
  then echo "script must be run as root"
  exit
fi

echo "start kubernets install script";

export KUBE_VERSION=1.28.2
export KUBE_CNI_VERSION=1.2.0
export CALICO_VERSION=v3.27.0

read -p "graphene username: " GRAPHENE_ADMIN
echo $GRAPHENE_ADMIN

swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

modprobe overlay
modprobe br_netfilter

tee /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF


# Add Repositorys and install Packages

sysctl --system

apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates

curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt update
apt install -y containerd.io

containerd config default |  tee /etc/containerd/config.toml >/dev/null 2>&1
sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

systemctl restart containerd
systemctl enable containerd

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg |  gpg --dearmour -o /etc/apt/trusted.gpg.d/kubernetes-xenial.gpg
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"

apt update
apt install -y --allow-downgrades kubernetes-cni=${KUBE_CNI_VERSION}-00 kubectl=${KUBE_VERSION}-00 kubelet=${KUBE_VERSION}-00 kubeadm=${KUBE_VERSION}-00
apt-mark hold kubelet kubeadm kubectl

ufw disable


# Kubernetes Cluster Settings

tmp=~/$(uuidgen)

kubeadm init --ignore-preflight-errors=all --pod-network-cidr=192.168.0.0/16 >>$tmp
cat $tmp
export k8s_joincmd=$(grep "kubeadm join" $tmp)
echo $k8s_joincmd >~/k8s_joincmd
rm $tmp

echo "[LOG] Cluster join command for manual use if needed: $k8s_joincmd";
echo "[LOG] Also saved in file ~/k8s_joincmd";

mkdir -p $HOME/.kube
cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config

tries=30
try=1
while ! kubectl taint node $HOSTNAME node.kubernetes.io/not-ready:NoSchedule- ; do
  echo "[LOG] Unable to remove not-ready taint from master; try $try of $tries";
  sleep 10
  try=$((try+1))
done


# Deploy pod network
echo "[LOG] Deploy calico as CNI";

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/tigera-operator.yaml
curl https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/custom-resources.yaml -O
kubectl create -f custom-resources.yaml


# Deploy kubernetes dashboard
echo "[LOG] Deploy kubernetes dashboard";

# makes sure that the pods can be assigned by the sheduler (fixes status "pending ")
kubectl taint nodes --all node-role.kubernetes.io/control-plane-

wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.1/aio/deploy/recommended.yaml
sed -i -e '1h;2,$H;$!d;g' -e 's~ports\:\n    - ~type\: NodePort\n  ports\:\n    - nodePort\: 32767\n      ~' recommended.yaml
sed -i '155,169d' recommended.yaml
kubectl create -f recommended.yaml

echo "[LOG] Enable default admin access to the kubernetes dashboard";

# per https://github.com/kubernetes/dashboard/wiki/Access-control#admin-privileges
cat <<EOF >dashboard-admin.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system
EOF
  
kubectl create -f dashboard-admin.yaml


# Complete setup
echo "[KUBERNETES] Setup is complete!";

apt-mark hold kubelet kubeadm kubectl kubernetes-cni
HOST_IP=$(/sbin/ip route get 8.8.8.8 | head -1 | sed 's/^.*src //' | awk '{print $1}')
echo "[LOG] The kubernetes dashboard is at https://$HOST_IP:32767"

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Integration of setup_helm script

function setup_helm() {
  trap 'fail' ERR
  echo "[HELM] Setup helm";
  wget https://get.helm.sh/helm-v3.12.3-linux-amd64.tar.gz
  tar -xvf helm-v3.12.3-linux-amd64.tar.gz
  cp linux-amd64/helm /usr/local/bin/helm
}

setup_helm
echo "[HELM] Setup is complete";

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

mkdir -p /home/$GRAPHENE_ADMIN/.kube
cp /etc/kubernetes/admin.conf /home/$GRAPHENE_ADMIN/.kube/config
chown -R $GRAPHENE_ADMIN:$GRAPHENE_ADMIN /home/$GRAPHENE_ADMIN/.kube

apt install docker docker-ce docker-ce-cli
sleep 65
systemctl start docker

